# Crystal-Ball

> ## Clone This Repository

> ## Change Directory to this project
>
> `cd Crystal-Ball`

> ## Set the Watch config
>
> For Linux `export watchDir=./toolkit/`
>
> For Windows `set watchDir=./toolkit/`

> # Launching Development Server
>
> `npm run start`

> # Building The Application
>
> `npm run build`


